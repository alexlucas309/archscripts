
#set locale+timezone
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
export LANG=en_US.UTF-8
ln -s /usr/share/zoneinfo/America/New_York /etc/localtime

hwclock --systohc --utc
timedatectl set-ntp true

#add cryptdevice to grub
sed -i 's#CMDLINE_LINUX="#&cryptdevice=/dev/sda2:luks_root#' /etc/default/grub

#add mkinitcpio encryption hook (initramfs)
sed -i 's/modconf block/& encrypt/' /etc/mkinitcpio.conf

#names and users setup
echo $NEWHOST > /etc/hostname
useradd -m -g users -G wheel,games,power,optical,storage,scanner,lp,audio,video -s /usr/bin/zsh $USERNAME
echo -e "$USERPASS\n$USERPASS" | passwd $USERNAME
echo -e "$USERPASS\n$USERPASS" | passwd root
sed -i '/%wheel/s/^#//g' /etc/sudoers
sed -i '/NOPASSWD/s/^/#/g' /etc/sudoers

#initramfs and bootloader
pacman -S linux linux-headers linux-firmware --noconfirm
echo -n "grub: "
grub-install --recheck /dev/$DISK
grub-mkconfig -o /boot/grub/grub.cfg

#misc setup
#systemctl enable NetworkManager #TODO add profiles, eg cli, graphical-base, graphical-full, server.
