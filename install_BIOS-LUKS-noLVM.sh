#!/bin/bash

if [ "$EUID" -ne 0 ]
then 
	echo "Please run as root."
	exit
fi

lsblk
echo "What disk do you want to install on?"
echo -n "=> "
read DISK
if [[ -z "$DISK" ]]
then
	DISK=null
fi
echo $DISK
if ! [ -e "/dev/"$DISK ]
then
	echo Could not find disk $DISK. Check spelling and try again.
	echo exit
	exit
fi
./addaline.sh $(echo "DISK=\\\""$DISK\\\") | sh
DISKSIZE=$(echo $(echo `bash v.sh $DISK | bash` \* 1024 | bc) | awk -F \. '{print $1}')
ROOTSIZE=`echo $DISKSIZE " - 128" | bc`

BOOTSIZE=256

echo Installing to ${DISKSIZE} MiB disk $DISK. Continue? \(y/N\)
echo -n "=> "
read CONTINUE1
if [[ -z "$CONTINUE1" ]]
then
	CONTINUE1=n
fi

if [ $CONTINUE1 != "y" ] && [ $CONTINUE1 != "Y" ]
then
	echo exit
	exit
fi
#--------------------------------------- stick read calls right under this line
echo How much space in Mib do you want to leave at the end of the disk?
echo -n "=> "
read TAILSIZE
if ! [ $TAILSIZE -eq 0 ]
then
	LEAVEFREE=true
fi

echo "Do you want to shred $DISK before installing? (Do this if there are already partitions present. More iterations is (probably) more secure. This will destroy all data on the disk.) (y/N)"
echo -n "=> "
read SHRED
if [ $SHRED = "y" ] || [ $SHRED = "Y" ]
then
	echo How many iterations?
	echo -n "=> "
	read ITERATIONS
	SHRED=true
fi

echo Input a passphrase for the encrypted root partition.
echo -n "=> "
read -s DISKPASS1
echo
echo Confirm the passphrase.
echo -n "=> "
read -s DISKPASS2
echo
until [ $DISKPASS1 = $DISKPASS2 ]
do
	echo "Those passphrases do not match."
	echo Input a passphrase for the encrypted root partition.
	echo -n "=> "
	read -s DISKPASS1
	echo
	echo Confirm the passphrase.
	echo -n "=> "
	read -s DISKPASS2
	echo
done
DISKPASS=$DISKPASS1
./addaline.sh $(echo "DISKPASS=\\\""$DISKPASS\\\") | sh

echo Input a username.
echo -n "=> "
read USERNAME
./addaline.sh $(echo "USERNAME=\\\""$USERNAME\\\") | sh

echo Input a user passphrase.
echo -n "=> "
read -s USERPASS1
echo
echo Confirm the passphrase.
echo -n "=> "
read -s USERPASS2
echo
until [ $USERPASS1 = $USERPASS2 ]
do
	echo "Those passphrases do not match."
	echo Input a passphrase for the encrypted root partition.
	echo -n "=> "
	read -s USERPASS1
	echo
	echo Confirm the passphrase.
	echo -n "=> "
	read -s USERPASS2
	echo
done
USERPASS=$USERPASS1
./addaline.sh $(echo "USERPASS=\\\""$USERPASS\\\") | sh

echo Input a hostname.
echo -n "=> "
read NEWHOST
./addaline.sh $(echo "NEWHOST=\\\""$NEWHOST\\\") | sh

./addaline.sh "#!/bin/bash" | sh

echo -e "\nEverything is automated from here on out. Go get yourself something to drink.\n"
#exit #uncomment this line to test user-input
sleep 1

if [ $SHRED = true ]
then
	echo Shredding $DISK...
	if [ "$ITERATIONS" -gt "1" ]
	then
		shred -vfn `echo $ITERATIONS " - 1" | bc` /dev/$DISK
		dd "if=/dev/zero" "of=/dev/$DISK" "bs=1M" "status=progress"
	else
		dd "if=/dev/zero" "of=/dev/$DISK" "bs=1M" "status=progress"
	fi
fi



timedatectl "set-ntp" "true"
modprobe dm-crypt dm-mod
pacman -Sy reflector --noconfirm
echo -n "Sorting pacman mirrors"
bash ./dotx3.sh
reflector --latest 50 --sort rate --protocol https --save /etc/pacman.d/mirrorlist
echo " done."

### Partioning ###

parted /dev/$DISK mklabel msdos -s
parted /dev/$DISK mkpart primary 1 `echo $BOOTSIZE" + 1" | bc`MiB -s
parted /dev/$DISK set 1 boot on -s
ROOTEND=`echo $DISKSIZE" - "$TAILSIZE" - 1" | bc`
ROOTEND=`echo $DISKSIZE" - "$TAILSIZE" - 1" | bc`
parted /dev/$DISK mkpart primary 269 ${ROOTEND}MiB -s

### Filesystems ###

echo Encrypting...

printf $DISKPASS | cryptsetup -v -s 512 -h whirlpool --cipher aes-xts-plain64 luksFormat /dev/${DISK}2 -d -
printf $DISKPASS | cryptsetup open /dev/${DISK}2 luks_root -d -

mkfs.ext4 /dev/${DISK}1 -L boot
mkfs.ext4 /dev/mapper/luks_root -L root

genfstab -U / > /etc/fstab

### Pacstrap ###

mount /dev/mapper/luks_root /mnt
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot
printf '\n\n\n' | pacstrap -i /mnt base base-devel iw dialog networkmanager dhcpcd nano neovim grub-bios os-prober zsh alacritty mkinitcpio mkinitcpio-busybox

### System ###

genfstab -U /mnt > /mnt/etc/fstab

cp chroot-dna.sh /mnt
chmod 777 /mnt/chroot-dna.sh
arch-chroot /mnt "./chroot-dna.sh"
rm /mnt/chroot-dna.sh
echo "Installation is complete. Do you want to reboot now? (Y/n)"
echo -n "=> "
read REBOOT
if [ $REBOOT != "n" ] && [ $REBOOT != "N" ] 
then
	umount /mnt/boot
	umount /mnt
	cryptsetup close luks_root
	systemctl reboot
fi
